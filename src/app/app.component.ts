import { Component, ViewChild } from '@angular/core';
import { Platform, ToastController, Config, NavController,Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FcmProvider } from '../providers/fcm/fcm';
import { Subject } from 'rxjs/Subject';
import { tap } from 'rxjs/operators';
import {messaging} from 'firebase/messaging'
import * as firebase from 'firebase/app';

import { TranslateService } from '@ngx-translate/core';
import { CookiesProvider } from '../providers/cookies/cookies';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:string = null;

  constructor(private cookies:CookiesProvider,private config: Config,private translate: TranslateService,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,fcm: FcmProvider, toastCtrl: ToastController) {
    this.checkIfPresent()
    platform.ready().then(() => {

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      fcm.listenToNotifications().pipe(
        tap(msg => {
          console.log(msg)
          // show a toast
          const toast = toastCtrl.create({
            message: msg.body,
            duration: 3000
          });
          toast.present();
        })
      )
      .subscribe();
      
      messaging.setBackgroundMessageHandler(function(payload) {
        // TODO: find a way to customise icon
        //TODO: handle click event
        var notificationTitle = payload.data.title;
        var notificationOptions = {
          body:payload.data.body,
          vibrate: [200, 100, 200, 100, 200, 100, 200],

        };
        console.log(notificationOptions)
      
        return messaging.showNotification(notificationTitle,
          notificationOptions);
      });

  
      this.initTranslate()

   


    });
    
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');

    //TODO: Change browserLang with device lang
  
      this.translate.use('en'); // Set your language here
    


  }
  checkIfPresent(){
  this.cookies.getProfileInfo().then(profile_inf=>{
    console.log(profile_inf)
  if(profile_inf!=null){
    this.rootPage='SecretsSwipePage';
  }else{
    this.rootPage='LoginPage'
  }
  })
  }
}

