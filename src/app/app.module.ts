import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SwingModule } from 'angular2-swing'
import { MyApp } from './app.component';
import { FIREBASE_CONFIG } from './firebase.credentials';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { NativePageTransitions} from '@ionic-native/native-page-transitions';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { SecretPageModule } from '../pages/secret/secret.module';
import { CreateSecretPageModule } from '../pages/create-secret/create-secret.module';
import { AutoResizeTextareaModule } from '../directives/auto-resize-textarea/auto-resize-textarea.module';
import { FcmProvider } from '../providers/fcm/fcm';
import { Firebase } from '@ionic-native/firebase';
import { GooglePlus } from '@ionic-native/google-plus';
import { Http, HttpModule } from '@angular/http';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { CookiesProvider } from '../providers/cookies/cookies';
import { IonicStorageModule } from '@ionic/Storage';

export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    MyApp,


  ],
  imports: [
    AngularFirestoreModule.enablePersistence(),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    HttpModule,
    
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot(),
    SwingModule, 
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    Firebase,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NativePageTransitions,
    ScreenOrientation,
    FcmProvider,
    GooglePlus,
    CookiesProvider
  ]
})
export class AppModule {
  constructor(private statusBar: StatusBar, private screenOrientation: ScreenOrientation) {this.statusBar.overlaysWebView(false);
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

    this.statusBar.backgroundColorByName('white');
  }

  
}
