import { IonicPage, NavController, NavParams, Events, Refresher, PopoverController, ToastController } from 'ionic-angular';
import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import * as firebase from 'firebase'
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import * as moment from 'moment';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { CookiesProvider } from '../../providers/cookies/cookies';

/**
 * Generated class for the SecretsSwipePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


/**
 * Generated class for the SecretsSwipePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-secrets-swipe',
  templateUrl: 'secrets-swipe.html',
})
export class SecretsSwipePage {


  cards=[];
  recentCard: string = '';
  profileData:any;
  currentIndex=0;
  allCards=[];
  numberElements=0;
  nextKey: any=undefined;
  endScroll=false;
  isLoading=false;
  isAnonymous=null;
  firstPull=true;
  lang=null;

  constructor(private cookie:CookiesProvider,private translate: TranslateService,public toastCtrl: ToastController,private statusBar: StatusBar, public popoverCtrl: PopoverController,private afauth : AngularFireAuth,public navCtrl: NavController,private afs: AngularFirestore,public events: Events) {
  }

  
  async googleauth(){


    var provider = new firebase.auth.GoogleAuthProvider();

    
    firebase.auth().currentUser.linkWithRedirect(provider).then((result)=> {
      this.logout()

    }).catch((error)=> {
      this.creationToast("There was an error, try again later")
  
    });

   
 
}

onItemSelection(selection){
  this.translate.use(selection);
  this.cookie.storeLangPref(selection);

}

async logout(){
  try{

   await this.afauth.auth.signOut()
    await this.cookie.deleteProfileInfo()
    this.navCtrl.setRoot('LoginPage')
  }catch{
    this.creationToast("There was an error, try again later")

  }
}

  
  countWords(s){
      s = s.replace(/(^\s*)|(\s*$)/gi,"");//exclude  start and end white-space
      s = s.replace(/[ ]{2,}/gi," ");//2 or more space to 1
      s = s.replace(/\n /,"\n"); // exclude newline with a start spacing
      return s.split(' ').length; 
  }

  ngAfterViewInit() {
    this.retrieveStoredProfile();
      this.cookie.getLangPref().then(lang=>{
        console.log(lang)

      if(lang!=null){
        this.translate.use(lang);
      }
    })
    

  }
  retrieveStoredProfile(){
    this.cookie.getProfileInfo().then(result=>{
      console.log(result)
      if (result!=null){
        this.profileData=JSON.parse(result);
      }else{
        this.retrieveProfile();
      }
    })

  }

  retrieveProfile() {
    return this.afauth.authState.take(1).subscribe(data=>{
      console.log(data)
      this.isAnonymous=data.isAnonymous
      return firebase.firestore().collection('profiles').doc(data.uid).get().then(result=>
      { 
      if(!result.exists){
        this.navCtrl.setRoot('ProfilePage')

      }else{
        this.profileData=result.data()
        this.cookie.storeProfileInfo(this.profileData)
        this.profileData.id=result.id;
        console.log(this.profileData)

         return this.profileData
      }
        }).catch(err=>{return console.error(err)})
    })
  }

  // Called whenever we drag an element


  // Connected through HTML







   getUpdate(){
     console.log('pulling')
    this.firstPull=false
    const query= firebase.firestore().collection('secrets').orderBy('creationDate','asc').startAfter(this.allCards[0].creationDate)
    const snapshot = query.onSnapshot(querySnapshot => {
      console.log(querySnapshot)
      querySnapshot.docChanges.forEach(snap => {

        if (snap.type === "added") {
          const data=snap.doc.data()
          data.id=snap.doc.id;
          data.momentTime=moment(data.creationDate).fromNow();
          this.allCards.unshift(data)

            this.creationToast("There are some new secrets!");

          
      }
      });
      
    })
  }

  creationToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  async showPopOver(myEvent) {
    let popover = this.popoverCtrl.create('ConfirmLogoutPage');

    if (this.isAnonymous){
      popover.present({
        ev: myEvent
      });
    }else{
      this.logout();
    }
  
  }


  async retrieveSecrets(){
    if (!this.endScroll){
    this.isLoading=true;
    var query=undefined;

    if (this.nextKey==undefined){
       query= firebase.firestore().collection('secrets').orderBy('creationDate','desc').limit(6).get();
    }else{
      query= firebase.firestore().collection('secrets').orderBy('creationDate','desc').startAfter(this.nextKey).limit(6).get();
    }

    const snapshot = await query;




      var secrets=[];

      snapshot.forEach(doc => {
        const data=doc.data();
        data.id=doc.id;
        data.momentTime=moment(data.creationDate).fromNow();
        secrets.push(data);
      });
      this.allCards.push.apply(this.allCards, secrets);


      if(this.firstPull){

        this.getUpdate()
      
      }


      this.numberElements=this.allCards.length
      console.log(this.allCards)
     
      try{
        this.nextKey=  new Date(secrets[(secrets.length)-1].creationDate);
        console.log(secrets)
        this.isLoading=false;

        }catch(err){
          this.nextKey=  new Date(this.allCards[0].creationDate);
          this.endScroll=true;
          this.isLoading=false;
        
        }    
      }


  }



  openModal(secret){
    let bookModal = this.navCtrl.push('SecretPage', { secret: secret,profile:this.profileData });

  }
  createSecret(){
    let bookModal = this.navCtrl.push('CreateSecretPage', {profile:this.profileData });

  }

}