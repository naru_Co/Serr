import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SecretsSwipePage } from './secrets-swipe';
import { SwingModule } from 'angular2-swing';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SecretsSwipePage,
  ],
  imports: [
    IonicPageModule.forChild(SecretsSwipePage),
    TranslateModule.forChild(),

    SwingModule,

  ],
})
export class SecretsSwipePageModule {}
