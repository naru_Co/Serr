import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectLangPopOverPage } from './select-lang-pop-over';

@NgModule({
  declarations: [
    SelectLangPopOverPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectLangPopOverPage),
  ],
})
export class SelectLangPopOverPageModule {}
