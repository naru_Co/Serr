import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SecretPage } from './secret';
import { DirectivesModule } from '../../directives/directives.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    SecretPage,

  ],
  imports: [
    IonicPageModule.forChild(SecretPage),
    TranslateModule.forChild(),
    DirectivesModule,
  ],
})
export class SecretPageModule {}
