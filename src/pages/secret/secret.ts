import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, PopoverController } from 'ionic-angular';
import * as firebase from 'firebase';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DocumentReference, WriteBatch } from '@firebase/firestore-types';
import * as moment from 'moment';

/**
 * Generated class for the SecretPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
//TODO: Add gif if no comments
@IonicPage()
@Component({
  selector: 'page-secret',
  templateUrl: 'secret.html',
})
export class SecretPage {

  secret:any;
  currentProfile:any;
  private comment: FormGroup;
  comments=[];
  istooLong:any;
  actualReaction:boolean=null;
  momentTime:any;
  isSending:boolean=false;
  constructor( public toastCtrl: ToastController,public navCtrl: NavController, public navParams: NavParams,private formBuilder: FormBuilder, public popoverCtrl: PopoverController) {
    this.comment = this.formBuilder.group({
      content: ['',Validators.compose([ Validators.required, Validators.maxLength(1500)]) ]
    });
    this.secret = navParams.get('secret');

    this.momentTime = moment(this.secret.creationDate).fromNow();

    if (this.secret.id && !this.secret.content){
      this.retrieveSecret(this.secret.id)
    }
    this.currentProfile=navParams.get('profile');
    console.log(this.currentProfile)
    this.getComments()
    this.getReaction();

  }
  async retrieveSecret(is){
    const secret = await firebase.firestore().collection('secrets').doc(this.secret.id).get();
    const id = this.secret.id
    this.secret=secret.data();
    this.secret.id=id;
    this.momentTime = moment(this.secret.creationDate).toNow();
    
  


  }
 


  async addMeToo(reaction){
    var batch = firebase.firestore().batch();
    var secretRef = firebase.firestore().collection('secrets').doc(this.secret.id).collection('metoo').doc(this.currentProfile.id);
    var profileRef=firebase.firestore().collection('profiles').doc(this.currentProfile.id).collection('metoos').doc(this.secret.id);
    if (this.actualReaction==null){
    batch.set(profileRef,{secretId:this.secret.id,secret:this.secret.title,reaction:reaction,creationDate: firebase.firestore.FieldValue.serverTimestamp()});
    batch.set(secretRef,{
      author:this.currentProfile.nickName, authorAvatar:this.currentProfile.avatar,reaction:reaction,creationDate: firebase.firestore.FieldValue.serverTimestamp(),secretId:this.secret.id
    });
  }else{
    batch.update(profileRef,{reaction:reaction});
    batch.update(secretRef,{reaction:reaction,creationDate: firebase.firestore.FieldValue.serverTimestamp() });
  }

    try{
    const result = await batch.commit()
    if(reaction){
    this.creationToast('Added to your Metoo secrets');
    }
    this.actualReaction=reaction;
  }catch(err){
    this.creationToast('There was a problem, try again later');
  }
  }

  async deleteReaction(){
    var batch = firebase.firestore().batch();
    var secretRef = firebase.firestore().collection('secrets').doc(this.secret.id).collection('metoo').doc(this.currentProfile.id)
    var profileRef = firebase.firestore().collection('profiles').doc(this.currentProfile.id).collection('metoos').doc(this.secret.id)
    batch.delete(secretRef);
    batch.delete(profileRef);
    try{
    batch.commit()
    this.actualReaction=null  
    }catch(err){
      this.creationToast('There was a problem, try again later');

    }
    }
     async getReaction(){
      const res = await firebase.firestore().collection('profiles').doc(this.currentProfile.id).collection('metoos').doc(this.secret.id).get();
      if (res.exists){
        this.actualReaction=res.data().reaction
      }
    }

    reactionControl(provenence){
      if (this.actualReaction===provenence) {
        if (provenence==true){
          this.secret.metoo=this.secret.metoo-1;
        }else{
          this.secret.nope=this.secret.nope-1;
        }
        this.deleteReaction()
      }else{
        this.addMeToo(provenence)
        if (this.actualReaction==null){

        if (provenence==true){
          this.secret.metoo=this.secret.metoo+1;
        }else{
          this.secret.nope=this.secret.nope+1;
        }

      }else{

        if (provenence==true){
          this.secret.metoo=this.secret.metoo+1;
          this.secret.nope=this.secret.nope-1;
        }else{
          this.secret.nope=this.secret.nope+1;
          this.secret.metoo=this.secret.metoo-1
        }

      }
      }

    }



  ionViewDidLoad() {
    console.log('ionViewDidLoad SecretPage');
  }

  showPopOver(myEvent) {
    let popover = this.popoverCtrl.create('PopOverPage');
    popover.present({
      ev: myEvent
    });
  }

  async addComment(form: NgForm) {
    this.isSending=true
    try{
    const query = await  firebase.firestore().collection('secrets').doc(this.secret.id).collection('comments').add({receiver:this.secret.author,
      author:this.currentProfile.nickName, content:this.comment.value.content, creationDate: firebase.firestore.FieldValue.serverTimestamp(), authorAvatar:this.currentProfile.avatar 
    })
    this.creationToast('Your Comment was added successfully');
    this.isSending=false
    this.comment.reset();


  }catch(err){
    this.creationToast(err);
    this.isSending=false
    this.comment.reset();


  }
  }

  async getComments(){
  
    var query =  firebase.firestore().collection('secrets').doc(this.secret.id).collection('comments').orderBy('creationDate','asc');
    var observer = query.onSnapshot(querySnapshot => {
      this.comments=[];
      querySnapshot.forEach(snap => {
        this.comments.unshift(snap.data())
      });

    });
    
  }

  creationToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}
