import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController } from 'ionic-angular';
import {AngularFireAuth} from 'angularfire2/auth' 
import * as firebase from 'firebase';
import { ToastController } from 'ionic-angular';
import { Button } from 'ionic-angular/components/button/button';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the CreateSecretPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
//TODO: Abillity to disable comments
@IonicPage()

@Component({
  selector: 'page-create-secret',
  templateUrl: 'create-secret.html',
})

export class CreateSecretPage {
  profileData:any;
  private secret: FormGroup;
  isLoading=false;
  constructor(private translate: TranslateService,public events: Events,private afauth : AngularFireAuth,public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController, public modalCtrl: ModalController,private formBuilder: FormBuilder,) {
    this.profileData=navParams.get('profile');

    this.secret = this.formBuilder.group({
      title: ['',Validators.compose([ Validators.required, Validators.maxLength(50), Validators.minLength(3)]) ],
      content:['',Validators.compose([ Validators.required, Validators.maxLength(3000), Validators.minLength(10)])]
    });

  }
  


  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateSecretPage');
  }

  categoriesModal() {
    let categoriesModal = this.modalCtrl.create('CategoriesModalPage');
    categoriesModal.present();
  }

  async creatSecret(form: NgForm) {
    try{
      this.isLoading=true;
    const query = await  firebase.firestore().collection('secrets').add({
      author:this.profileData.nickName, title:this.secret.value.title, content:this.secret.value.content, creationDate: firebase.firestore.FieldValue.serverTimestamp(), authorAvatar: this.profileData.avatar ? this.profileData.avatar : null,reactionsCount:0,metoo:0,nope:0})
      this.isLoading=false;

        this.creationToast('Your Secret was added successfully');

      this.secret.reset();
      this.navCtrl.push('SecretPage', { secret: {id :query.id},profile:this.profileData }).then(()=>{
        this.navCtrl.remove(this.navCtrl.getPrevious().index);

      })
  }catch(err){
    this.creationToast(err);
  }
   
  }

  creationToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  

}
