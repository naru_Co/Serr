import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateSecretPage } from './create-secret';
import { AutoResizeTextareaDirective } from '../../directives/auto-resize-textarea/auto-resize-textarea';
import { DirectivesModule } from '../../directives/directives.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    CreateSecretPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateSecretPage),
    TranslateModule.forChild(),
    DirectivesModule
  ],
})
export class CreateSecretPageModule {}
