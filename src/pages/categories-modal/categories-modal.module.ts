import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriesModalPage } from './categories-modal';

@NgModule({
  declarations: [
    CategoriesModalPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoriesModalPage),

  ],
})
export class CategoriesModalPageModule {}
