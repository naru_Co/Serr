import { Component } from '@angular/core';
import { NavController, IonicPage,Refresher } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as _ from "lodash";
import * as firebase from 'firebase';
import { Timestamp } from '@firebase/firestore-types';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { errorHandler } from '@angular/platform-browser/src/browser';
import { error } from '@firebase/database/dist/esm/src/core/util/util';
@IonicPage()

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  profileData:any;
  secrets=[];
  currentUser:any;
  secret:any={};
  numberItems = 5;
  nextKey: any;
  prevKeys: any[] = [];
  subscription: any;
  timestamp:any;
  endScroll=true;
  itemsCollection:any;
  items:any;
  

  constructor(private afauth : AngularFireAuth,public navCtrl: NavController,private afs: AngularFirestore) {

    this.retrieveProfile();
    this.retrieveSecrets();


  }

  async doRefresh(refresher: Refresher) {
    var query= firebase.firestore().collection('secrets').orderBy('creationDate','desc').limit(5).get();
    const snapshot = await query
    this.secrets=[];
    var secrets=[];

    snapshot.forEach(doc => {
        secrets.unshift(doc.data());
    });
    this.secrets.push.apply(this.secrets, secrets);
    refresher.complete();


      try{
        this.nextKey=  new Date(secrets[0].creationDate);
        }catch(err){
          this.nextKey=undefined;
          this.endScroll=false;
        
        }
   }


   doPulling(refresher: Refresher) {}
  

  getAgain(infiniteScroll){
    if (this.endScroll){
    this.retrieveSecrets(this.nextKey,infiniteScroll)
    }else{
      infiniteScroll.enable(false);
    }
  }

  retrieveProfile() {
    return this.afauth.authState.take(1).subscribe(data=>{
      this.currentUser=data;
      return firebase.firestore().collection('profiles').doc(data.uid).get().then(result=>
      { return this.profileData=result.data()}).catch(err=>{return console.error(err)})
    })
  }
  async createSecret(secret) {

    const query =  this.afs.collection('secrets').add({
      author:this.profileData.nickName, title:secret.title, content:secret.content, creationDate: firebase.firestore.FieldValue.serverTimestamp() 
    })
  
  try{
    const docRef = await query
    return this.secrets.unshift(secret)
  } catch(err){
    console.error(err)
  }

  }



  async retrieveSecrets(startKey?,infiniteScroll?){
    var query=undefined;

    if (startKey==undefined){
      startKey=""
       query= firebase.firestore().collection('secrets').orderBy('creationDate','desc').limit(5).get();
    }else{
      query= firebase.firestore().collection('secrets').orderBy('creationDate','desc').startAfter(startKey).limit(5).get();
    }

    const snapshot = await query;
      var secrets=[];

      snapshot.forEach(doc => {
        secrets.unshift(doc.data());
      });
      this.secrets.push.apply(this.secrets, secrets);
      if(infiniteScroll){
        infiniteScroll.complete();
      }

      try{
        this.nextKey=  new Date(secrets[0].creationDate);
        }catch(err){
          this.nextKey=undefined;
          this.endScroll=false;
        
        }    
  }



}
