import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import {AngularFireAuth} from 'angularfire2/auth' 
import * as firebase from 'firebase/app';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { Observable } from 'rxjs/Observable';
import { GooglePlus } from '@ionic-native/google-plus';
import { Platform } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import {CookiesProvider} from '../../providers/cookies/cookies';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  user : any;
  profile=true;
  lang=null;
  isAnonymous=null;
  isLoading:boolean=false;
  constructor(private cookies:CookiesProvider, private translate: TranslateService,public popoverCtrl: PopoverController,private afauth : AngularFireAuth, public navCtrl: NavController, public navParams: NavParams,private nativePageTransitions: NativePageTransitions,private platform: Platform) {
    

    this.afauth.authState.take(1).subscribe((user)=> {
      if (user) {
        this.isAnonymous=user.isAnonymous
        this.user=true;
        this.retrieveProfile(user)
      } else {
      }
    });
    

  }


  async showPopOver(myEvent) {
    let popover = this.popoverCtrl.create('ConfirmLogoutPage');

    if (this.isAnonymous){
      popover.present({
        ev: myEvent
      });
    }else{
      await this.afauth.auth.signOut()
      this.user= false;

    }
  
  }

  ionViewDidLoad() {
    this.isLoading=true;

    //TODO: Add animation on loading
  
    firebase.auth().getRedirectResult().then((result)=> {
      this.isLoading=false;

      if (result.credential) {
        this.isLoading=true;
        console.log(result)
        this.user=true
       this.retrieveProfile(result.user,true)
      }
    }).catch((error)=> {
      this.isLoading=false;

      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
    });
   
  }

  googleLogin() {

      this.nativeGoogleLogin();

  }


  async nativeGoogleLogin(): Promise<void> {


      var provider = new firebase.auth.GoogleAuthProvider();

  
      firebase.auth().signInWithRedirect(provider).then(()=> {
        return firebase.auth().getRedirectResult();
      }).then((result)=> {
        this.retrieveProfile(result.user,true)

        console.log("is it ?")
        console.log(result)
        this.user=true
      }).catch((error)=> {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
      });

     
   
  }






  retrieveProfile(data,isGoogle?) {
    this.isLoading=true;
    console.log(isGoogle)
      return firebase.firestore().collection('profiles').doc(data.uid).get().then(result=>
      { 
      if(!result.exists){
        
        this.profile=false;
        console.log("nope")
        console.log(isGoogle)
        if(isGoogle){
          this.navCtrl.setRoot('ProfilePage')
        }
      }else{
        console.log(result)
        this.navCtrl.setRoot('SecretsSwipePage')
        this.isLoading=false;


      }
    }).catch(err=>{
      this.isLoading=false;
      return console.error(err)})
  }

  async loginAnonymously(){
    try{
    const result = await this.afauth.auth.signInAnonymously()
    if (result){
      this.navCtrl.setRoot('ProfilePage')
    }
    }
    catch(e){
      console.error(e);
    }
  }

 



 
  ClickExpolore(){
    if (this.user){
      if(this.profile){
      this.GoToHomePage()
      }else{
        this.navCtrl.setRoot('ProfilePage')
      }
    }else{
      this.loginAnonymously()
    }
  }


  GoToHomePage() {

    /* let options: NativeTransitionOptions = {
      direction: 'up',
      duration: 500,
      slowdownfactor: 3,
      slidePixels: 20,
      iosdelay: 100,
      androiddelay: 150,
      fixedPixelsTop: 0,
      fixedPixelsBottom: 60
     };
    this.nativePageTransitions.slide(options); */

    this.navCtrl.setRoot('SecretsSwipePage')

  }

  ionViewWillLeave() {

   
  
   
   }
  



}
