import { Component, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {AngularFireAuth} from 'angularfire2/auth' 
import { AngularFireDatabase} from 'angularfire2/database'
import { auth } from 'firebase/app';
import * as firebase from 'firebase';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import {
  StackConfig,
  DragEvent,
  SwingStackComponent,
  SwingCardComponent
} from 'angular2-swing';
import { FormGroup, FormBuilder, Validators, NgForm, AbstractControl, ValidationErrors } from '@angular/forms';
import 'rxjs/add/observable/timer';
import { Observable } from 'rxjs/Observable';
import { FcmProvider } from '../../providers/fcm/fcm';
import {CookiesProvider} from "../../providers/cookies/cookies";

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  @ViewChild('myswing1') swingStack: SwingStackComponent;
  @ViewChildren('mycards1') swingCards: QueryList<SwingCardComponent>;
  private profile: FormGroup;
  images:any;
  cards=[];
  stackConfig: StackConfig;
  currentIndex=0;
  rots=[];
  firstTime=true;
  nickNameAvailable=true;
  isLoading=false;

  constructor(private fcm: FcmProvider,private coockies:CookiesProvider, private formBuilder: FormBuilder,private afauth : AngularFireAuth, public navCtrl: NavController, public navParams: NavParams, private afDatabase: AngularFireDatabase) {
    this.stackConfig = {
      throwOutConfidence: (offset, element: any) => {
        return Math.min(Math.abs(offset) / (element.offsetWidth / 2), 1);
      },
      transform: (element, x, y, r) => {
        this.onItemMove(element, x, y, r);
      },
      throwOutDistance: (d) => {
        return 800;
      }
    };
    this.getAvatars();
    this.profile = this.formBuilder.group({
      nickName: ['',Validators.compose([ Validators.required,Validators.minLength(4), Validators.maxLength(15),Validators.pattern('[A-Za-z0-9]*')])]
    });
  }

async getAvatars(){
const snapshot = await firebase.database().ref('/image-paths').once('value');
this.images=snapshot.val()
this.images.forEach(element => {
  this.rots.push(this.randomRot());
});
this.images.reverse();
}





 async validateNickName(nickname) {
  
  const exist = await firebase.firestore().collection('profiles').where("nickName", "==", nickname.toLowerCase()).get()
  
  if (exist.empty) {
    this.nickNameAvailable=true;
    return true
  }else{
    this.nickNameAvailable=false;

    return false

  }
  
  
}
  ionViewDidLoad() {
    this.swingStack.throwin.subscribe((event: DragEvent) => {
    });

  }
   async createProfile(form: NgForm) {
    this.isLoading=true;
    const isavailable = await this.validateNickName(this.profile.value.nickName)
    if (isavailable){

      const avatar = this.images[this.currentIndex];
      console.log(this.currentIndex)
      var profile=null;
      profile={nickName:null,avatar:null};
      profile.nickName=this.profile.value.nickName.toLowerCase();
      profile.avatar=avatar;
      //TODO: to fix with async 
      this.afauth.authState.take(1).subscribe(auth=>{
        firebase.firestore().collection('profiles').doc(auth.uid).set(profile).then(()=>{
          profile.id=auth.uid
          this.navCtrl.setRoot('SecretsSwipePage')
          this.coockies.storeProfileInfo(profile)
          this.fcm.getToken(profile.nickName);

        })
      })
    }

   
  }

 

  randomRot(){
    var rot = Math.floor((Math.random() * 5) + 0);
    rot *= Math.floor(Math.random()*2) == 1 ? 1 : -1;
     return {transform: " rotate("+rot+"deg)"}
   }

   onItemMove(element, x, y, r) {
    let color = '';
    const abs = Math.abs(x);
    const min = Math.trunc(Math.min(16 * 16 - abs, 16 * 16));

    element.style['transform'] = `translate3d(0, 0, 0) translate(${x}px, ${y}px) rotate(${r}deg)`;
  }

  goRight() {
    this.firstTime=false;
    this.currentIndex++
    this.currentIndex<=5 ? this.currentIndex : this.currentIndex=0 ;

    var last = this.images[0];
    this.images.shift();
    this.images.push(last);
    console.log(this.currentIndex)

    
  
  }

  

  goLeft(){
    this.firstTime=false;
    this.currentIndex--
    this.currentIndex>=0 ? this.currentIndex : this.currentIndex=5 ;

    var first = this.images[this.images.length-1];
    this.images.pop();
    this.images.unshift(first);
    console.log(this.currentIndex)


   
  }

}
