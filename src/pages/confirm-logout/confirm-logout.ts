import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { CookiesProvider } from '../../providers/cookies/cookies';

/**
 * Generated class for the ConfirmLogoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-logout',
  templateUrl: 'confirm-logout.html',
})
export class ConfirmLogoutPage {

  constructor(private cookies:CookiesProvider,public viewCtrl: ViewController,public toastCtrl: ToastController,private afauth : AngularFireAuth,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmLogoutPage');
  }

  async logout(){
    try{

     await this.afauth.auth.signOut()
     this.viewCtrl.dismiss().then(()=>{
       this.cookies.deleteProfileInfo();
      this.navCtrl.setRoot('LoginPage')
    });
    }catch{
      this.creationToast("There was an error, try again later")

    }


  }

  creationToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  async googleauth(){


    var provider = new firebase.auth.GoogleAuthProvider();

    
    firebase.auth().currentUser.linkWithRedirect(provider).then(function(result) {
      this.logout()

    }).catch(function(error) {
      this.creationToast("There was an error, try again later")
  
    });

   
 
}

}
