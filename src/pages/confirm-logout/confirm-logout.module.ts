import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmLogoutPage } from './confirm-logout';

@NgModule({
  declarations: [
    ConfirmLogoutPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmLogoutPage),
  ],
})
export class ConfirmLogoutPageModule {}
