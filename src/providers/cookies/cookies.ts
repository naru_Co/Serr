import { Injectable } from '@angular/core';
import {Storage} from "@ionic/Storage";

/*
  Generated class for the CookiesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CookiesProvider {

  constructor(private readonly storage: Storage,) {
    console.log('Hello CookiesProvider Provider');
  }

   storeProfileInfo(profile) {
     console.log(JSON.stringify(profile))
     const profilestr=JSON.stringify(profile);
    return this.storage.set('profile_inf', profilestr);
  }
  storeLangPref(lang){
    return this.storage.set('lang_pref',lang);
  }
  getLangPref():Promise<any> {
    return this.storage.get('lang_pref')
  }
  getProfileInfo() :Promise<any>{
    return this.storage.get('profile_inf')

  }
  deleteProfileInfo() :Promise<any>{
    return this.storage.clear();
  }


}
