import { Directive , HostListener, ElementRef, Input, OnInit, NgModule} from '@angular/core';

/**
 * Generated class for the AutoResizeTextareaDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[auto-resize-textarea]' // Attribute selector
})
export class AutoResizeTextareaDirective implements OnInit {
  @HostListener('input', ['$event.target'])
  onInput(textArea: HTMLTextAreaElement): void {
      this.adjust();
  }

  @Input('auto-resize-textarea') maxHeight: number;


  constructor(public element: ElementRef) {}

  ngOnInit(): void {
    setTimeout(() => {
        this.adjust();
    }, 500);
}

adjust(): void {
    let ta = this.element.nativeElement.querySelector("textarea");
    if (ta) {
        ta.style.overflow = "hidden";
        ta.style.height = null;
        ta.style.height = Math.min(ta.scrollHeight, this.maxHeight) + "px";
    } else {
        this.element.nativeElement.style.overflow = "hidden";
        this.element.nativeElement.height = null;
        this.element.nativeElement.style.height = Math.min(this.element.nativeElement.scrollHeight, this.maxHeight) + "px";
    }
}
}
