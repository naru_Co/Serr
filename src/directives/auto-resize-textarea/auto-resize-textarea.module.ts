import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutoResizeTextareaDirective } from '../../directives/auto-resize-textarea/auto-resize-textarea';
import {CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

@NgModule({
  declarations: [
  ],
  exports:[
  ]
})
export class AutoResizeTextareaModule {}
