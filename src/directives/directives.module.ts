import { NgModule } from '@angular/core';
import { AutoResizeTextareaDirective } from './auto-resize-textarea/auto-resize-textarea';
@NgModule({
	declarations: [AutoResizeTextareaDirective],
	imports: [],
	exports: [AutoResizeTextareaDirective]
})
export class DirectivesModule {}
